# Bienvenue sur le projet annuel de l'ESGI

### Contexte

- Réalisation de zéro d'un CMS utilisant PHP/HTML/CSS

# Association Editor

![](https://i.ibb.co/9bFqWDg/Logo-Makr-3e0-LDo.png)

![](https://img.shields.io/badge/ESGI-CMS__project-green.svg) ![](https://img.shields.io/badge/IW2-Association-lightgrey) ![](https://img.shields.io/badge/version-1.0.0-yellow)

Version (with date):

    | Version       | 	  Date	    |
    | ------------- | ------------- |
    | 1.0.0		    | 11/12/2019    |
    | 1.1.0		    | 20/01/2020    |
    | 2.0.0		    | 13/02/2020    |
    | 2.1.0		    | 15/02/2020    |


###  Task

- [ ] Take style in view
- [ ] Test views
- [x] Graphic chart
- [ ] Model
- [ ] Administration
- [ ] DataBase
- [ ] ???
    
## Built With

* [Jquery](https://jquery.com/) - bibliothèque JavaScript libre

## Contributing

Please read [CONTRIBUTING.md](https://) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [GitLab](https://gitlab.com) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Maxime** - *Admin* - [GitLab](https://gitlab.com/maxime.caulet)

* **Jeremy** - *Admin* - [GitLab](https://gitlab.com/)

* **Thibault** - *Admin* - [GitLab](https://gitlab.com/thibault1.duval)

* **Caroline** - *Admin* - [GitLab](https://gitlab.com/Saaryn)

* **Enzo** - *Admin* - [GitLab](https://gitlab.com/enzo.gelin)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License
[MIT]()
