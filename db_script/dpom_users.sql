-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : databaseCMS:3306
-- Généré le :  ven. 15 mai 2020 à 13:42
-- Version du serveur :  5.7.28
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mvc-pa`
--

-- --------------------------------------------------------

--
-- Structure de la table `dpom_users`
--

CREATE TABLE `dpom_users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(16) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `token` tinytext,
  `date_inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dpom_users`
--

INSERT INTO `dpom_users` (`id`, `firstname`, `lastname`, `email`, `pwd`, `status`, `token`, `date_inserted`, `date_updated`) VALUES
(3, 'Bob', 'Miluo', 'email@email.fr', 'azertyuiop', 1, NULL, '2020-02-26 21:05:34', '2020-05-03 20:50:57'),
(4, 'Enzo', 'GELIN', 'enzo.gelin.18@gmail.com', 'azertyuio4', 2, NULL, '2020-02-08 10:24:05', '2020-02-16 14:01:04'),
(5, 'Enzo', 'GELIN', 'enzo.gelin.18@gmail.com', 'azertyuio4', 2, NULL, '2020-02-08 18:01:22', '2020-02-16 14:01:06'),
(6, 'Enzo', 'GELIN', 'enzo.gelin.18@gmail.com', 'azertyuiop', 0, NULL, '2020-02-08 18:13:32', '2020-02-16 14:01:08'),
(7, 'Enzo', 'GELIN', 'enzo.gelin.d1d8@gmail.com', 'azertyuiop', 0, NULL, '2020-02-16 00:01:13', '2020-02-16 14:01:09'),
(8, 'Enzo', 'GELINCCC', 'enzo.gelin.dd1d8@gmail.com', '0', 1, NULL, '2020-02-26 21:05:40', NULL),
(9, 'Enzo', 'GELIN', 'enzo.gelin.dd1d8@gmail.com', 'azertyuiop', 0, NULL, '2020-02-16 00:04:21', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dpom_users`
--
ALTER TABLE `dpom_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dpom_users`
--
ALTER TABLE `dpom_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
