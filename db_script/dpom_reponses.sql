-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : databaseCMS:3306
-- Généré le :  ven. 15 mai 2020 à 13:42
-- Version du serveur :  5.7.28
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mvc-pa`
--

-- --------------------------------------------------------

--
-- Structure de la table `dpom_reponses`
--

CREATE TABLE `dpom_reponses` (
  `id` int(11) NOT NULL,
  `id_exercice` int(11) NOT NULL,
  `num_question` int(11) NOT NULL,
  `num_reponse` int(11) NOT NULL,
  `reponse` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dpom_reponses`
--

INSERT INTO `dpom_reponses` (`id`, `id_exercice`, `num_question`, `num_reponse`, `reponse`, `status`) VALUES
(66, 9, 1, 1, 'rep 1 f', 0),
(67, 9, 1, 2, 'rep 1 f', 0),
(68, 9, 1, 3, 'rep 1 v', 1),
(69, 9, 2, 1, 'rep 2 f', 0),
(70, 9, 2, 2, 'rep 2 v', 1),
(71, 9, 2, 3, 'rep 2 f', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dpom_reponses`
--
ALTER TABLE `dpom_reponses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dpom_reponses`
--
ALTER TABLE `dpom_reponses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
