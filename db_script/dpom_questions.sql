-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : databaseCMS:3306
-- Généré le :  ven. 15 mai 2020 à 13:42
-- Version du serveur :  5.7.28
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mvc-pa`
--

-- --------------------------------------------------------

--
-- Structure de la table `dpom_questions`
--

CREATE TABLE `dpom_questions` (
  `id` int(11) NOT NULL,
  `id_exercice` int(11) NOT NULL,
  `num_question` int(11) NOT NULL,
  `question` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dpom_questions`
--

INSERT INTO `dpom_questions` (`id`, `id_exercice`, `num_question`, `question`) VALUES
(33, 9, 1, 'Question 1'),
(34, 9, 2, 'Question 2'),
(35, 9, 1, 'Question 1'),
(36, 9, 1, 'Question 1'),
(37, 9, 1, 'Question 1'),
(38, 9, 1, 'Question 1'),
(39, 9, 1, 'Question 1'),
(40, 9, 1, 'Question 1'),
(41, 9, 1, 'Question 1'),
(42, 9, 1, 'Question 1'),
(43, 9, 1, 'Question 1'),
(44, 9, 1, 'Question 1'),
(45, 9, 1, 'Question 1'),
(46, 9, 1, 'Question 1'),
(47, 9, 1, 'Question 1'),
(48, 9, 1, 'Question 1'),
(49, 9, 1, 'Question 1'),
(50, 9, 1, 'Question 1'),
(51, 9, 1, 'Question 1'),
(52, 9, 1, 'Question 1'),
(53, 9, 1, 'Question 1'),
(54, 9, 1, 'Question 1'),
(55, 9, 1, 'Question 1'),
(56, 9, 1, 'Question 1'),
(57, 9, 1, 'Question 1'),
(58, 9, 1, 'Question 1'),
(59, 9, 1, 'Question 1'),
(60, 9, 1, 'Question 1'),
(61, 9, 1, 'Question 1'),
(62, 9, 1, 'Question 1'),
(63, 9, 1, 'Question 1'),
(64, 9, 1, 'Question 1'),
(65, 9, 2, 'Question 2'),
(66, 9, 2, 'Question 2'),
(67, 9, 2, 'Question 2'),
(68, 9, 2, 'Question 2'),
(69, 9, 2, 'Question 2'),
(70, 9, 2, 'Question 2'),
(71, 9, 2, 'Question 2'),
(72, 9, 2, 'Question 2'),
(73, 9, 2, 'Question 2'),
(74, 9, 2, 'Question 2'),
(75, 9, 2, 'Question 2'),
(76, 9, 2, 'Question 2'),
(77, 9, 2, 'Question 2'),
(78, 9, 2, 'Question 2'),
(79, 9, 2, 'Question 2'),
(80, 9, 2, 'Question 2'),
(81, 9, 2, 'Question 2'),
(82, 9, 2, 'Question 2'),
(83, 9, 2, 'Question 2'),
(84, 9, 2, 'Question 2'),
(85, 9, 2, 'Question 2'),
(86, 9, 2, 'Question 2'),
(87, 9, 2, 'Question 2'),
(88, 9, 2, 'Question 2'),
(89, 9, 2, 'Question 2'),
(90, 9, 2, 'Question 2'),
(91, 9, 2, 'Question 2'),
(92, 9, 2, 'Question 2'),
(93, 9, 2, 'Question 2'),
(94, 9, 2, 'Question 2'),
(95, 9, 2, 'Question 2'),
(96, 9, 2, 'Question 2'),
(97, 9, 2, 'Question 2'),
(98, 9, 2, 'Question 2'),
(99, 9, 2, 'Question 2'),
(100, 9, 2, 'Question 2');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dpom_questions`
--
ALTER TABLE `dpom_questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dpom_questions`
--
ALTER TABLE `dpom_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
