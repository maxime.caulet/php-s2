-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : databaseCMS:3306
-- Généré le :  ven. 15 mai 2020 à 13:42
-- Version du serveur :  5.7.28
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mvc-pa`
--

-- --------------------------------------------------------

--
-- Structure de la table `dpom_pages`
--

CREATE TABLE `dpom_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT 'Page',
  `content` text,
  `order_page` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `dpom_pages`
--

INSERT INTO `dpom_pages` (`id`, `name`, `content`, `order_page`) VALUES
(1, 'Page', '<p>azertyuiopddddddddddddddazertyuiosssssazertyuiopeeeeeeeeeeeeeeeeeeee</p><p>&nbsp;</p>', 1),
(2, 'page acc', 'edrftgyhujikolp', 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `dpom_pages`
--
ALTER TABLE `dpom_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_page` (`order_page`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `dpom_pages`
--
ALTER TABLE `dpom_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
