#!/bin/sh
echo "Welcome to the PA config files"
echo "        .
      ":"
    ___:____     |'\/'|
  ,          .    \  /
  |  O        \___/  |
~^~^~^~^~^~^~^~^~^~^~^~^~
  Running Docker Environement"
sleep 4
gnome-terminal -e 'sh -c "bash logdocker.sh"'
echo "Open in net : " ;
echo "
__         __         __         __         __   ___    
       _
             | |
             | |===( )   //////
             |_|   |||  | o o|
                    ||| ( c  )                  ____
                     ||| \= /                  ||   \_
                      ||||||                   ||     |
                      ||||||                ...||__/|-
                      ||||||             __|________|__
                        |||             |______________|
                        |||             || ||      || ||
                        |||             || ||      || ||
------------------------|||-------------||-||------||-||-------
                        |__>            || ||      || ||

"
sleep 10
xdg-open http://localhost:85
xdg-open http://localhost:8085
echo "end"
exit

