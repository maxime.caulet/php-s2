<?php
namespace App\models;

use App\core\DB;

class Guests extends DB
{
    protected $id;
    protected $surname;
    protected $ip;
    protected $date_inserted;
    protected $date_updated;

    public function __construct()
    {
        parent::__construct();
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function setSurname($surname)
    {
        $this->surname = ucwords(strtolower(trim($surname)));
    }
    public function setIp($ip)
    {
        $this->ip = $ip;
    }
    public function setDateInsert($date_inserted)
    {
        $this->date_inserted = $date_inserted;
    }
    public function setDateUpdate($date_updated)
    {
        $this->date_updated = $date_updated;
    }
    public static function getSurname()
    {
        // array of random words
        $listSurname = array("Kay","Joe","Susan","Frank","Norbert","Alfonzo","Lenard","Carmela","Tameka","Debra","Lina","Suzette","Amy","Non","Nolan","Von","Raquel","Carole","Johnnie","Morgan","Jules","Larry","Megan","Bruyère","Olin","Leigh","Nanette","Inez","Kristy","Léon","Wayne","Trey","Marguerite","Reynaldo","Longue","Liz","Leila","Ezequiel","Josue","Horacio","Sam","Phil","Nora","Jonathan","Lela","Tony","Armando","Léa","Gwendolyn","Cyril","Mamie","Leroy","Harold","Jon","Sydney","Wilford","Alphonse","Leopoldo","Kayla","Jessie","Jean","Samantha","Norberto","Letitia","Jacqueline","Naomi","Hester","Nellie","Lola","Wilber","Peter","Espérer","Mohammad","Damon","Thad","Lindsey","Bourgeon","Rayford","Norma","Samual","Imelda","Callie","Benita","Barrett","Lorena","Tyree","Kerry","Dario","Nadine","Lula","Ester","Marcus","Francis","Sonja","Tanneur","Natalie","grand vent","Tracey","quartier","Stewart","Monique","Minnie","Christina","Julia");
        // get random value from array $listSurname
        $randIndex = array_rand($listSurname);
        // output the value for the random index
        return $listSurname[$randIndex];
    }

}
