<?php
namespace App\models;

use App\core\DB;

class Reponses extends DB
{
    protected $id;
    protected $id_exercice;
    protected $num_question;
    protected $num_reponse;
    protected $reponse;
    public $status;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setIdExercice($id_exercice)
    {
        $this->id_exercice = $id_exercice;
    }
    public function setNumeroQuestion($num_question)
    {
        $this->num_question = $num_question;
    }
    public function setNumeroReponse($num_reponse)
    {
        $this->num_reponse = $num_reponse;
    }
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;
    }
    public function setStatus($status)
    {
        $this->status = $status;
    }
    public static function getRep()
    {
        return ["id"=>$_GET['id']];
    }
}
