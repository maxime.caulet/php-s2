<?php
namespace App\models;

use App\core\DB;

class Astuces extends DB
{
    protected $id;
    protected $title;
    protected $content;
    protected $date;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }
    public static function getLess()
    {
        return ["id"=>$_GET['id']];
    }
}
