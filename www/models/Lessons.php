<?php
namespace App\models;

use App\core\DB;

class Lessons extends DB
{
    protected $id;
    protected $title;
    protected $content;
    protected $id_categorie;
    protected $date;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setTitle($title)
    {
        $this->title = $title;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
    public function setIdCategorie($id_categorie)
    {
        $this->id_categorie = $id_categorie;
    }
    public function setDate($date)
    {
        $this->date = $date;
    }
    public static function getLess()
    {
        return ["id"=>$_GET['id']];
    }
}
