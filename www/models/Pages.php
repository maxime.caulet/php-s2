<?php
namespace App\models;

use App\core\DB;
use App\core\Helpers;

class Pages extends DB
{
    protected $id;
    protected $name;
    protected $content;
    protected $order_page;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setContent($content)
    {
        $this->content = $content;
    }
    public function setOrder($order_page)
    {
        $this->order_page = $order_page;
    }
    public static function getPage()
    {
        return ["id"=>$_GET['id']];
    }
    public static function getPageForm()
    {
        return [
                "config"=>[
                    "method"=>"POST",
                    "class"=>"pages",
                    "action"=>Helpers::getUrl("Page", "one"),
                    "id"=>"",
                    "submit"=>"Enregistrer"],
                "fields"=>[
                    "name"=>[
                            "type"=>"text",
                            "required"=>true,
                            "placeholder"=>"name",
                            "class"=>"",
                            "id"=>"name",
                            "name"=>"name",
                            "minlenght"=>2,
                            "maxlenght"=>50,
                            "errMsg"=>"Nom incorrect"
                    ],
                    "editor"=>[
                            "type"=>"textarea",
                            "required"=>true,
                            "placeholder"=>"editor",
                            "class"=>"",
                            "id"=>"editor",
                            "name"=>"editor",
                            "value"=>"",
                            "minlenght"=>2,
                            "errMsg"=>"Content error"
                    ],
                    "order_page"=>[
                            "type"=>"number",
                            "required"=>true,
                            "class"=>"",
                            "id"=>"order_page",
                            "name"=>"order_page",
                            "value"=>"",
                            "errMsg"=>"error nunmber order"

                    ]
                ]
        ];
    }
}
