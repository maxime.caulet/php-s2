<?php
namespace App\models;

use App\core\DB;

class Questions extends DB
{
    protected $id;
    protected $id_exercice;
    protected $num_question;
    protected $question;

    public function __construct()
    {
        parent::__construct();
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setIdExercice($id_exercice)
    {
        $this->id_exercice = $id_exercice;
    }
    public function setNumeroQuestion($num_question)
    {
        $this->num_question = $num_question;
    }
    public function setQuestion($question)
    {
        $this->question = $question;
    }
    public static function getQuest()
    {
        return ["id"=>$_GET['id']];
    }
    public static function getQuestionnaire(){
        return ["id_exercice"=>$_GET['id']];
    }
}
