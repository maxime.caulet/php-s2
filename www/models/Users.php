<?php
namespace App\models;

use App\core\DB;
use App\core\Helpers;

class Users extends DB
{
    protected $id;
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $pwd;
    protected $status;
    protected $date_inserted;
    protected $date_updated;
    protected $token;

    public function __construct()
    {
        parent::__construct();
    }

    //factory method : methode de création d'une instance de l'objet, à partir de données
    //méthode pour transformer le retour de la DB en objet user (pour le token)
    public static function of($data){
        $user = new Users();

        $user->setId($data["id"]);
        $user->setFirstname($data["firstname"]);
        $user->setLastname($data["lastname"]);
        $user->setEmail($data["email"]);
        $user->setPwd($data["pwd"]);
        $user->setStatus($data["status"]);
        $user->setDateInsert($data["date_inserted"]);
        $user->setDateUpdate($data["date_updated"]);
        $user->setToken($data["token"]);

        return $user;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    public function setFirstname($firstname)
    {
        $this->firstname = ucwords(strtolower(trim($firstname)));
    }
    public function setLastname($lastname)
    {
        $this->lastname = strtoupper(trim($lastname));
    }
    public function setEmail($email)
    {
        $this->email = strtolower(trim($email));
    }
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;
    }
    public function setStatus($status)
    {
        $this->status = $status;
    }
    public function setDateInsert($date_inserted)
    {
        $this->date_inserted = $date_inserted;
    }
    public function setDateUpdate($date_updated)
    {
        $this->date_updated = $date_updated;
    }
    public function setToken($token) {
        $this->token = $token;
    }

    public function getToken() {
        return $this->token;
    }

    public static function getRegisterForm()
    {
        return [
                "config"=>[
                    "method"=>"POST",
                    "class"=>"user",
                    "action"=>Helpers::getUrl("User", "register"),
                    "id"=>"",
                    "submit"=>"S'inscrire"],
                "fields"=>[
                    "firstname"=>[
                            "type"=>"text",
                            "required"=>true,
                            "placeholder"=>"Votre prénom",
                            "class"=>"",
                            "id"=>"firstname",
                            "minlenght"=>2,
                            "maxlenght"=>50,
                            "errMsg"=>"Votre prénom est incorrect"
                    ],
                    "lastname"=>[
                            "type"=>"text",
                            "required"=>true,
                            "placeholder"=>"Votre nom",
                            "class"=>"",
                            "id"=>"lastname",
                            "value"=>"",
                            "minlenght"=>2,
                            "maxlenght"=>100,
                            "errMsg"=>"Votre nom est incorrect"
                    ],
                    "email"=>[
                            "type"=>"email",
                            "required"=>true,
                            "placeholder"=>"email@mail.fr",
                            "class"=>"",
                            "id"=>"email",
                            "value"=>"",
                            "errMsg"=>"Votre email est incorrect"

                    ],
                    "password"=>[
                            "type"=>"password",
                            "required"=>true,
                            "placeholder"=>"**********",
                            "class"=>"",
                            "id"=>"pwd",
                            "minlenght"=>8,
                            "value"=>"",
                            "errMsg"=>"Votre password est incorrect"
                    ],
                    "passwordConfirm"=>[
                            "type"=>"password",
                            "required"=>true,
                            "placeholder"=>"**********",
                            "class"=>"",
                            "id"=>"passwordconfirm",
                            "value"=>"",
                            "confirmWith"=>"password",
                            "errMsg"=>"Votre 2eme password est incorrect"
                    ],
                    "captcha"=>[
                            "type"=>"captcha",
                            "required"=>true,
                            "class"=>"",
                            "id"=>"captcha",
                            "placeholder"=>"Saisir le texte",
                            "errMsg"=>"Captcha est incorrect"
                    ]
                ]
        ];
    }
    public static function getLoginForm()
    {
        return [
            "config"=>[
                "method"=>"POST",
                "class"=>"user",
                "action"=>Helpers::getUrl("User", "login"),
                "id"=>"",
                "submit"=>"Se connecter"],
            "fields"=>[
                "email"=>[
                        "type"=>"email",
                        "required"=>true,
                        "placeholder"=>"email@mail.fr",
                        "class"=>"",
                        "id"=>"email",
                        "value"=>"",
                        "errMsg"=>"Votre email est incorrect"

                ],
                "password"=>[
                        "type"=>"password",
                        "required"=>true,
                        "placeholder"=>"**********",
                        "class"=>"",
                        "id"=>"pwd",
                        "value"=>"",
                        "errMsg"=>"Votre password est incorrect"
                ]
            ]
        ];
    }
    public static function getOneUser()
    {
        return ["email"=>$_POST['email'],"pwd"=>$_POST['password']];
    }
    public static function getUs()
    {
        return ["id"=>$_GET['id']];
    }

    public static function getOneByToken()
    {
        return ["token"=>"\"".$_SESSION['token']."\""];
    }
}
