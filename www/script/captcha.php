<?php 
//enzo.gelin-CAPTCHA

header("Content-type: image/png");
// Basics variables
$width = 500;
$height = 150;
$number_shape = rand(3,6);

//Captcha code generation
$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789";
$result_str = substr(str_shuffle($chars),6, rand(6,8));
$result_formatted = implode('   ',str_split($result_str));
$final_str = "";

//servira a la validation
$_SESSION['captcha'] = $result_str;

//Gestion of fonts
$dir = "fonts/*.ttf"; //search a path for only .ttf files
$font_file = glob($dir);
$final_font = $font_file[rand(0,count($font_file)-1)];//count-1 car debut a 0



$image = imagecreatetruecolor($width, $height);
//colors gestion
$back_color = imagecolorallocate($image, rand(0,128),rand(0,128),rand(0,128));
$font_color = imagecolorallocate($image, rand(128,255),rand(128,255),rand(128,255));

imagefilledrectangle($image, 0, 0, $width, $height, $back_color);

// function which returns the value in RGB of a hexadecimal color
function hexargb($hex) {
    return array(
        "r" => hexdec(substr($hex,0,2)),
        "g" => hexdec(substr($hex,2,2)),
        "b" => hexdec(substr($hex,4,2))
    );
};

// Gestion of shapes
for( $i = 0; $i <= $number_shape; $i++ ){
    $rgb = hexargb(substr(str_shuffle($chars),6,8)); // random color with $chars
    imageline($image, rand(1, $width - 25), rand(1, $height), rand(1, $width + 25), rand(1, $height), imagecolorallocate($image, $rgb['r'], $rgb['g'], $rgb['b']));
    imageellipse($image, rand(1,200), rand(1, 150), rand(1, 200), rand(1, 200), imagecolorallocate($image, $rgb['r'], $rgb['g'], $rgb['b']));
    imagerectangle($image, rand(1,200), rand(1, 150), rand(190, 400), rand(1, 200), imagecolorallocate($image, $rgb['r'], $rgb['g'], $rgb['b']));
}

//final generation
imagettftext($image, 20, rand(-9, 9), rand(50,130), rand(70, 85) , $font_color, $final_font, $result_formatted);
imagepng($image);



