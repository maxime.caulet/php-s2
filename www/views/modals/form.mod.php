<?php
$dataSubmit = $GLOBALS["_" . $data["config"]["method"]];
?>

<form method="<?= $data["config"]["method"] ?>"
      action="<?= $data["config"]["action"] ?>"
      class="<?= $data["config"]["class"] ?>"
      id="<?= $data["config"]["id"] ?>">

    <div class="form-group row">


        <?php foreach ($data["fields"] as $name => $configField) : ?>
            <div class="col-sm-12">


                <?php if ($configField["type"] == "captcha"): ?>
                    <img src="script/captcha.php" alt="" width="15%">
                <?php endif; ?>


                <input
                        type="<?= $configField["type"] ?>"
                        name="<?= $name ?>"
                        placeholder="<?= isset($configField["placeholder"]) ? $configField["placeholder"] : '' ?>"
                        class="<?= isset($configField["class"]) ? $configField["class"] : '' ?>"
                        required="<?= isset($configField["required"]) ? $configField["required"] : '' ?>"
                        id="<?= isset($configField["id"]) ? $configField["placeholder"] : '' ?>"
                    <?= ($configField["required"]) ? "required='required'" : "" ?>
                        value="<?= (!empty($dataSubmit[$name]) && $configField["type"] != "password" && $configField["type"] != "captcha") ? $dataSubmit[$name] : '' ?>">

            </div>
        <?php endforeach; ?>

    </div>
    <button class="btn-primary"><?= $data["config"]["submit"] ?></button>
</form>
