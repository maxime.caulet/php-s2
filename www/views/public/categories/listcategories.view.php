<?php
use App\core\Helpers;
?>
<div class="row">
    <?php foreach($categories as $row => $value): ?>
        <div class="col-sm-6 col-md-3">
            <div class="lesson_thumb">
                <div class="header_thumb">
                    <div class="title_thumb">
                        <i><?=$value['name'];?></i>
                    </div>
                </div>
                <div class="description_thumb"><i>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat debitis voluptatibus soluta?</i></div>
                <div class="container-fluid">
                    <div class="row">
                        <!-- <div class="col-sm-8 col-md-10 right_thumb"><a class="btn_thumb" id="btn_lesson" href="<?= Helpers::getUrl("Exercice", "unique");echo("?id=".$value['id']); ?>">Acceder</a></div> -->
                        <!-- <div class="col-sm-4 col-md-2"><button class="btn2_thumb"><i class="far fa-star fa-2x yellow"></i></button></div> -->
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>