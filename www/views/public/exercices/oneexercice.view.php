<div class="row content_block">
    <div class="col-md-12">
        <div class="content_title">
            <p><?= $exercice['title'] ?></p>
        </div>
    </div>
    <div class="col-sm-12 col-md-9">
        <div class="content_component">
            <form>
                <div class="question">
                    <div class="texte">
                        <h2>Question 1</h2>
                        Le code jQuery s'exécute :<br>
                        <input type="radio" id="r1" name="q1">Dans le navigateur<br>
                        <input type="radio" id="r2" name="q1">Sur le serveur où est stocké le code<br>
                        <input type="radio" id="r3" name="q1">Tantôt dans le navigateur, tantôt sur le serveur<br>
                        <br><span class="reponse" id="reponse1">Le code jQuery n'est autre que du JavaScript. A ce titre, il s'exécute toujours sur les clients (ordinateurs, tablettes et téléphones) qui font référence à ce code via une page HTML. La bonne réponse est donc la première.</span>
                    </div>  
                    <img id="img1" src="question.png" />
                </div>
                <a class="button_rep_quest" href="">Tester les réponses</a>
            </form>
        </div>
    </div>
    <div class="col-sm-2 col-md-2 summary_col">
        <!-- <div class="content_summary">
            <p>Sommaire : </p>
            <ul>
                <li><a class="content_summary_link" href="#0">Introduction</a></li>
                <li><a class="content_summary_link" href="#1">Qu'est ce l'histoire</a>
                    <ol>
                        <li><a class="content_summary_link" href="#1_1">Une enquête sur le passé</a></li>
                        <li><a class="content_summary_link" href="#1_2">Les démarches de la méthode historique</a></li>
                    </ol>
                </li>
                <li><a class="content_summary_link" href="#2">Les périodes historiques</a>
                    <ol>
                        <li><a class="content_summary_link" href="#2_1">Les repères en histoire</a></li>
                        <li><a class="content_summary_link" href="#2_2">Les quatres périodes historiques</a></li>
                    </ol>
                </li>
                <li><a class="content_summary_link" href="#3">Conclusion</a></li>
            </ul>
        </div> -->
    </div>
</div>