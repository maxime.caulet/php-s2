<section class="section1">
    <img id="section1Mobile" src="public/assets/images/section1/SECTION-01.svg" alt="section 1">
    <img id="section1-pc" src="public/assets/images/section1/SECTION-01-pc.svg" alt="section 1">
</section>

<section class="section2" id="valeurs">
    <img id="section2Pc" src="public/assets/images/section2/SECTION-02-pc.svg" alt="section 2">
    <img id="section2Mobile" src="public/assets/images/section2/SECTION-02-mobile.svg" alt="section 2">
    <img id="section2Bis" src="public/assets/images/section2/SECTION-02-BIS.svg" alt="section 2">
    <img id="section2Ter" src="public/assets/images/section2/SECTION-02-TER.svg" alt="section 3">
</section>

<section class="section3" id="mission">
    <img id="section3Mobile" src="public/assets/images/section3/SECTION-03.svg" alt="section 3">
    <img id="section3Pc" src="public/assets/images/section3/SECTION-03Pc.svg" alt="section 3">
    <img id="section3Ter" src="public/assets/images/section2/SECTION-02-TER.svg" alt="section 3">
    <img id="section3BisPc" src="public/assets/images/section3/SECTION3Bis-pc.svg" alt="section 3">
</section>

<section class="section4">
    <img id="section4-Mobile" src="public/assets/images/section4/SECTION-04.svg" alt="section 4">
    <img id="section4Bis-Mobile" src="public/assets/images/section4/SECTION-04-BIS.svg" alt="section 4">
</section>
