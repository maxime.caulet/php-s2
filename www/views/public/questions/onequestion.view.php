<div class="row content_block">
    <div class="col-md-12">
        <div class="content_title">
            <p><?= $question['title'] ?></p>
        </div>
    </div>
    <div class="col-sm-12 col-md-9">
        <div class="content_component">
            <p><?= $question['content'] ?></p>
        </div>
    </div>
    <div class="col-sm-2 col-md-2 summary_col">
        <div class="content_summary">
            <p>Sommaire : </p>
            <ul>
                <li><a class="content_summary_link" href="#0">Introduction</a></li>
                <li><a class="content_summary_link" href="#1">Qu'est ce l'histoire</a>
                    <ol>
                        <li><a class="content_summary_link" href="#1_1">Une enquête sur le passé</a></li>
                        <li><a class="content_summary_link" href="#1_2">Les démarches de la méthode historique</a></li>
                    </ol>
                </li>
                <li><a class="content_summary_link" href="#2">Les périodes historiques</a>
                    <ol>
                        <li><a class="content_summary_link" href="#2_1">Les repères en histoire</a></li>
                        <li><a class="content_summary_link" href="#2_2">Les quatres périodes historiques</a></li>
                    </ol>
                </li>
                <li><a class="content_summary_link" href="#3">Conclusion</a></li>
            </ul>
        </div>
    </div>
</div>