<div class="container_card">
    <?php foreach($lessons as $row => $value): ?>
        <div class="cards_card">
            <input type="hidden" class="id_lesson" name="id_lesson" value="<?= $value['id'];?>">
            <h3 class="title_card"><?=$value['title'];?></h3>
            <div class="bar_card">
                <div class="emptybar_card"></div>
                <div class="filledbar_card"></div>
            </div>
            <div class="circle_card">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="stroke_card">
                <circle class="stroke_card" cx="60" cy="60" r="50"/>
                <text x="35" y="60">Access</text>
                </svg>
                <!-- <svg height="90" class="stroke_card" width="200">
                <circle class="stroke_card" cx="60" cy="60" r="50"/>
                <text x="25" y="20">I love SVG</text>
            </svg> -->
            </div>
        </div>
    <?php endforeach;?>
</div>