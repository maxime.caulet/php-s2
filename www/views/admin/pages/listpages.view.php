<?php use App\core\Helpers; ?>
<table id="myTable" class="display">
  <caption>list of pages</caption>
  <thead>
    <tr>
      <th>Id</th>
      <th>name</th>
      <th>content</th>
      <th>order</th>
      <th>access pages</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($pages as $row => $value): ?>
        <tr>
            <td><?=$row;?></td>
            <td><?=$value['name'];?></td>
            <td><?php echo(strip_tags($value['content']));?></td>
            <td><?=$value['order_page'];?></td>
            <td><a href="<?= Helpers::getUrl("Page", "one");echo("?id=".$value['id']) ?>">link to <?php echo($value['name']);?></a></td>
        </tr>
    <?php endforeach;?>
  </tbody>
</table>