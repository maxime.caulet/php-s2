<?php use App\core\Helpers; ?>
<table id="myTable" class="display">
  <caption>list of questions</caption>
  <thead>
    <tr>
      <th>Id</th>
      <th>title</th>
      <th>Questions</th>
      <th>date</th>
      <th>access questions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($questions as $row => $value): ?>
        <tr>
            <td><?=$row;?></td>
            <td><?=$value['title'];?></td>
            <td><?=$value['question'];?></td>
            <td><?=$value['date'];?></td>
            <td><button><a href="<?= Helpers::getUrl("Question", "one");echo("?id=".$value['id']) ?>">Edit</a></button></td>
        </tr>
    <?php endforeach;?>
  </tbody>
</table>