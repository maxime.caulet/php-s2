<?php use App\core\Helpers; ?>
<form action="<?= Helpers::getUrl("Question", "one") ?>" method="post">
    <input type="hidden" name="id" value="<?= $question['id'] ?>">
    <div>
        <label for="title">Title : </label>
        <input type="text" name="title" value="<?= $question['title'] ?>">
    </div>
    <div>
        <label for="content">Content :</label>
        <textarea id="editor" name="editor" rows="5" cols="33">
            <!-- <input type="text" name="editor"> -->
            <?= $question['question'] ?>
        </textarea>
    </div>
    <div>
        <button type="submit">Enregistrer</button>
    </div>
</form>