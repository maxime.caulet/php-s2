<?php use App\core\Helpers; ?>
<form action="<?= Helpers::getUrl("Lesson", "one") ?>" method="post">
    <input type="hidden" name="id" value="<?= $lesson['id'] ?>">
    <div>
        <label for="title">Title : </label>
        <input type="text" name="title" value="<?= $lesson['title'] ?>">
    </div>
    <div>
        <label for="id_categorie">Catégorie : </label>
        <select name="id_categorie" id="id_categorie">
                <option value="0">--Please choose an option--</option>
            <?php foreach($categories as $row => $value): ?>
                <option value="<?=$value['id'];?>" <?php if($lesson['id_categorie'] == $value['id']){ echo('selected');} ?> ><?=$value['name'];?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div>
        <label for="content">Content :</label>
        <textarea id="editor" name="editor">
            <!-- <input type="text" name="editor"> -->
            <?= $lesson['content'] ?>
        </textarea>
    </div>
    <div>
        <button type="submit">Enregistrer</button>
    </div>
</form>