<?php use App\core\Helpers; ?>
<table id="myTable" class="display">
  <caption>list of lessons</caption>
  <thead>
    <tr>
      <th>Id</th>
      <th>firstname</th>
      <th>lastname</th>
      <th>email</th>
      <th>status</th>
      <th>date_inserted</th>
      <th>Button</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($users as $row => $value): ?>
        <tr>
            <td><?=$row;?></td>
            <td><?=$value['firstname'];?></td>
            <td><?=$value['lastname'];?></td>
            <td><?=$value['email'];?></td>
            <td><?=$value['status'];?></td>
            <td><?=$value['date_inserted'];?></td>
            <td><button><a href="<?= Helpers::getUrl("User", "one");echo("?id=".$value['id']) ?>">Edit</a></button>|<button onclick="delet($value['id'])">Supprimer</button></td>
        </tr>
    <?php endforeach;?>
  </tbody>
</table>