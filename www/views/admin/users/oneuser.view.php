<?php use App\core\Helpers; ?>
<form action="<?= Helpers::getUrl("User", "one") ?>" method="post">
    <input type="hidden" name="id" value="<?= $user['id'] ?>">
    <div>
        <label for="firstname">firstname : </label>
        <input type="text" name="firstname" value="<?= $user['firstname'] ?>">
    </div>
    <div>
        <label for="lastname">lastname :</label>
        <input type="text" name="lastname" value="<?= $user['lastname'] ?>">
    </div>
    <div>
        <label for="email">email :</label>
        <input type="text" name="email" value="<?= $user['email'] ?>">
    </div>
    <div>
        <label for="pwd">pwd :</label>
        <input type="text" name="pwd" value="<?= $user['pwd'] ?>">
    </div>
    <div>
        <label for="status">status :</label>
        <input type="text" name="status" value="<?= $user['status'] ?>">
    </div>
    <div>
        <button type="submit">Enregistrer</button>
    </div>
</form>