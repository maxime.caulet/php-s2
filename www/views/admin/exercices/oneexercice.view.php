<?php use App\core\Helpers; ?>
<form action="<?= Helpers::getUrl("Exercice", "one") ?>" method="post">
    <input type="hidden" name="id" value="<?= $exercice['id'] ?>">
    <div>
        <label for="title">Title : </label>
        <input type="text" name="title" value="<?= $exercice['title'] ?>">
    </div>
    <div>
        <label for="id_categorie">Catégorie : </label>
        <select name="id_categorie" id="id_categorie">
                <option value="0">--Please choose an option--</option>
            <?php foreach($categories as $row => $value): ?>
                <option value="<?=$value['id'];?>" <?php if($exercice['id_categorie'] == $value['id']){ echo('selected');} ?> ><?=$value['name'];?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div>
        <label for="content">Content :</label>
        <textarea id="editor" name="editor">
            <!-- <input type="text" name="editor"> -->
            <?= $exercice['content'] ?>
        </textarea>
    </div>
    
    <?php if(isset($teub)){ ?>
        <?php foreach($questions as $row => $value): ?>
            
            <div id="quest_number_<?= $value['num_question'] ?>">
                <fieldset>
                    <label for="question_detail">Question : <input type="text" name="intitule_question_<?= $value['num_question'] ?>" size="80" value="<?= $value['question'] ?>"> ?</label>
                    <!-- <?php $this->addModal("reponses", "data")?> -->
                </fieldset>
            </div> 
        <?php endforeach;?>
    <?php } else { ?> 
    <div>
        <label for="nb_question">Questions : </label>
        <input type="number" name="nb_question" id="nb_question" value="0" min="0" max="5">
        <a class="bouton_default" id="bouton_default" onclick="addQuestion();" href="#">Ajouter des questions (QCM)</a>
    </div>
    <div class="questions_created_form" id="questions_created_form">
        
    </div>
    <?php } ?>
    <div>
        <button type="submit">Enregistrer</button>
    </div>
</form>