<?php use App\core\Helpers; ?>
<table id="myTable" class="display">
  <caption>list of lessons</caption>
  <thead>
    <tr>
      <th>Id</th>
      <th>title</th>
      <th>content</th>
      <th>date</th>
      <th>access lessons</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($exercices as $row => $value): ?>
        <tr>
            <td><?=$value['id'];?></td>
            <td><?=$value['title'];?></td>
            <td><?php echo(substr(strip_tags($value['content']), 0, 150));?></td>
            <td><?=$value['date'];?></td>
            <td><a href="<?= Helpers::getUrl("Exercice", "one");echo("?id=".$value['id']) ?>">link to <?php echo($value['title']);?></a></td>
        </tr>
    <?php endforeach;?>
  </tbody>
</table>