<?php print_r($_SESSION); 

use App\core\Helpers;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="public/css/dist/style.css">
</head>
<body>

<header>
    <a href="#" id="logo"><img src="public/assets/images/LogoNavPc.png" alt="logo"></a>
    <a href="#" id="logo-mobile"><img src="public/assets/images/Logo-menu-mobile.svg" alt="logo-mobile"></a>
    <img id="burger" onclick="burger()" src="public/assets/images/burger.svg" alt="burger">
    <i id="quit" onclick="quit()">clear</i>

    <div class="links" id="links">
        <a href="#valeurs">Nos valeurs</a>
        <a href="#mission">Notre Mission</a>
        <!-- <a href="#">Plugins</a> -->
        <a href="<?= Helpers::getUrl("User", "login") ?>">Connexion</a>
    </div>
</header>

<?php include "views/".$this->view.".view.php";?>  

<script>
    function burger() {
        var burger = document.getElementById('burger');
        var links = document.getElementById('links');
        var quit = document.getElementById('quit');
        var logo = document.getElementById('logo');
        burger.style.padding = '10% 5% 3% 3%';
        links.style.display = 'flex';
        quit.style.display = 'inline';
        logo.style.display = 'none';
    }

    function quit() {
        var burger = document.getElementById('burger');
        var links = document.getElementById('links');
        var quit = document.getElementById('quit');
        //var logo = document.getElementById('logo');
        burger.style.padding = '10% 5% 3% 3%';
        links.style.display = 'none';
        quit.style.display = 'none';
        //logo.style.display = 'block';
    }
</script>
</body>
</html>