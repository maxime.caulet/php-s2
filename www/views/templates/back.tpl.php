<?php use App\core\Helpers; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Template Back</title>
    <link rel="stylesheet" href="/public/css/backstylephemere.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script src="public/css/ckeditor.js"></script>
</head>
<body>
<nav class="nav__cont">
    <ul class="nav">
        <li class="nav__items ">
            <img src="/public/assets/images/blog.png"></img>
            <a href="<?= Helpers::getUrl("Exercice", "list") ?>">Exercices</a>
        </li>
        <li class="nav__items ">
            <img src="/public/assets/images/users.png" viewBox="0 0 32 35.6">
                <use xlink:href="#planner"></use>
            </img>
            <a href="<?= Helpers::getUrl("User", "list") ?>">Users</a>
        </li>

        <li class="nav__items ">
            <img src="/public/assets/images/pages.png" viewBox="0 0 32 35.6">
                <use xlink:href="#planner"></use></img>
            <a href="<?= Helpers::getUrl("Page", "list") ?>">Pages</a>
        </li>

        <li class="nav__items ">
            <img src="/public/assets/images/pages.png" viewBox="0 0 32 35.6">
                <use xlink:href="#planner"></use></img>
            <a href="<?= Helpers::getUrl("Question", "list") ?>">Questions</a>
        </li>

        <li class="nav__items ">
            <img src="/public/assets/images/pages.png" viewBox="0 0 32 35.6">
                <use xlink:href="#planner"></use></img>
            <a href="<?= Helpers::getUrl("Lesson", "list") ?>">Lessons</a>
        </li>

    </ul>
</nav>

<div class="wrapper">
    <main>
        <div class="article">
            <?php include "views/".$this->view.".view.php";?>
        </div>
    </main>
</div>

<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
                console.log( editor );
        } )
        .catch( error => {
                console.error( error );
        } );
</script>
<script src="/public/vendor/jquery/jquery.min.js"></script>
<script src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="/public/css/backscriptephemere.js"></script>
</body>
</html>