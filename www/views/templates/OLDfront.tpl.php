<?php
use App\core\Helpers;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Template Front</title>
    <link type="text/css" rel="stylesheet" href="/public/css/stylephemere.css">
    <link  type="text/css" rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<!-- <link type="text/css" href="/public/css/dist/style.css" rel="stylesheet"></link> -->
</head>
	<!-- <nav class="topMenu">
		<a href="#"></a>
		<ul>
			<li id="before1"><a href="/">Nos valeurs</a></li>
			<li id="before2"><a href="<?= Helpers::getUrl("Lesson", "panel") ?>">Cours</a></li>
			<li id="before3"><a href="<?= Helpers::getUrl("Question", "panel") ?>">Questions</a></li>
            <?php if(isset($_SESSION['logon'])){ ?>
                <?php if($_SESSION['status'] == 1){ ?>
                    <li id="before5"><a href="<?= Helpers::getUrl("User", "default") ?>">My account settings</a></li>
                <?php } ?>
                <li id="before4"><a href="<?= Helpers::getUrl("User", "logout") ?>">Loggout</a></li>
            <?php } else { ?>
                <li id="before4"><a href="<?= Helpers::getUrl("User", "login") ?>">Connexion</a></li>
            <?php } ?>
		</ul>
	</nav> -->
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="col-inner">
                <button class="btn -top -info js-sidebar-expand">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
		</div>
    </div>
    <div class="row">
        <!-- left sidebar -->
		<div class="col-sm-3 left_sidebar_test">
			<div class="col-inner left_sidebar">Col-3</div>
        </div>
        <!-- Content -->
        <div class="col-sm-9">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="col-inner">
                            <div class="menu-box block"> 
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
<script src="/public/vendor/jquery/jquery.min.js"></script>
<script src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="/public/css/scriptephemere.js"></script>
</body>
</html>
