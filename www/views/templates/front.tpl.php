<?php
error_log(print_r($_SESSION,true)); 
use App\core\Helpers;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Template Front</title>
    <link type="text/css" rel="stylesheet" href="/public/css/stylecss.css">
    <link  type="text/css" rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<!-- <link type="text/css" href="/public/css/dist/style.css" rel="stylesheet"></link> -->
</head>
<body>


<div class="wrapper">
    <header class="header">
        <div class="logo col-lg-1">
        <a href="#">
            School Site
        </a>
        </div>
        <div class="toolbar col-lg-11">
            <button class="btn btn-default">
            <i class="fa fa-align-right" aria-hidden="true"></i>
            </button>
            <div class="toggle-bar">
            <div class="panel"></div>
            <div class="panel"></div>
            <div class="panel"></div>
            </div>
        </div>
    </header>
    <aside class="left-sidebar">
        <div class="toggle-sidebar">
            <div class="toggle-bar">
                <button>
                    <i class="fa fa-indent fa-lg icon-right"></i>
                </button>
            </div>
            <div class="setting">
                <button>
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <span><i class="fas fa-ellipsis-v"></i></span>
                </button>
                <div class="auth">
                    <ul>
                        <li><a href="#">Mon compte</a></li>
                        <li class="reg">
                            <a href="<?= Helpers::getUrl("User", "logout") ?>">Déconnexion</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <nav class="nav">
            <ul class="main-menu">
                <li class="home dashboard">
                    <a href="<?= Helpers::getUrl("Lesson", "panel") ?>">
                        <i class="fas fa-book" aria-hidden="true"></i>
                        <span>Cours</span>
                    </a>
                    <div class="sub-menu">
                        <ul>
                            <li>
                                <a href="<?= Helpers::getUrl("Lesson", "panel") ?>">Menu Cours</a>
                            </li>
                            <li>
                                <a href="#">Favoris</a>
                            </li>
                            <li>
                                <a href="<?= Helpers::getUrl("Categorie", "panel") ?>">Catégories</a>
                            </li>
                            <li>
                                <a href="#">Inventory</a>
                            </li>
                            <li>
                                <a href="#">Reserve</a>
                            </li>
                            <li>
                                <a href="#">Loaction</a>
                            </li>
                            <li>
                                <a href="#">Worehouse</a>
                            </li>
                            <li>
                                <a href="#">Available balance</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="blog dashboard">
                    <a href="<?= Helpers::getUrl("Exercice", "panel") ?>">
                        <i class="fas fa-graduation-cap" aria-hidden="true"></i>
                        <span>Exercices</span>
                    </a>
                    <div class="sub-menu">
                        <ul>
                            <li>
                                <a href="<?= Helpers::getUrl("Exercice", "panel") ?>">Menu Exercices</a>
                            </li>
                            <li>
                                <a href="#">Favoris</a>
                            </li>
                            <li>
                                <a href="#">Catégories</a>
                            </li>
                            <li>
                                <a href="#">Inventory</a>
                            </li>
                            <li>
                                <a href="#">Reserve</a>
                            </li>
                            <li>
                                <a href="#">Loaction</a>
                            </li>
                            <li>
                                <a href="#">Worehouse</a>
                            </li>
                            <li>
                                <a href="#">Available balance</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="services dashboard">
                    <a href="<?= Helpers::getUrl("Question", "panel") ?>">
                    <i class="fas fa-question" aria-hidden="true"></i>
                        <span>Questions</span>
                    </a>
                    <div class="sub-menu">
                        <ul>
                            <li>
                                <a href="<?= Helpers::getUrl("Question", "panel") ?>">Menu Questions</a>
                            </li>
                            <li>
                                <a href="#">Assebmling</a>
                            </li>
                            <li>
                                <a href="#">Delivery</a>
                            </li>
                            <li>
                                <a href="#">Inventory</a>
                            </li>
                            <li>
                                <a href="#">Reserve</a>
                            </li>
                            <li>
                                <a href="#">Loaction</a>
                            </li>
                            <li>
                                <a href="#">Worehouse</a>
                            </li>
                            <li>
                                <a href="#">Available balance</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="portfolio dashboard">
                    <a href="<?= Helpers::getUrl("Astuce", "panel") ?>">
                        <i class="fas fa-brain" aria-hidden="true"></i>
                        <span>Astuces</span>
                    </a>
                    <div class="sub-menu">
                        <ul>
                            <li>
                                <a href="<?= Helpers::getUrl("Astuce", "panel") ?>">Menu Astuces</a>
                            </li>
                            <li>
                                <a href="#">Assebmling</a>
                            </li>
                            <li>
                                <a href="#">Delivery</a>
                            </li>
                            <li>
                                <a href="#">Inventory</a>
                            </li>
                            <li>
                                <a href="#">Reserve</a>
                            </li>
                            <li>
                                <a href="#">Loaction</a>
                            </li>
                            <li>
                                <a href="#">Worehouse</a>
                            </li>
                            <li>
                                <a href="#">Available balance</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="contact dashboard">
                    <a href="#">
                        <i class="fas fa-toolbox" aria-hidden="true"></i>
                        <span>Services</span>
                    </a>
                    <div class="sub-menu">
                        <ul>
                            <li>
                                <a href="#">Menu Services</a>
                            </li>
                            <li>
                                <a href="#">Assebmling</a>
                            </li>
                            <li>
                                <a href="#">Delivery</a>
                            </li>
                            <li>
                                <a href="#">Inventory</a>
                            </li>
                            <li>
                                <a href="#">Reserve</a>
                            </li>
                            <li>
                                <a href="#">Loaction</a>
                            </li>
                            <li>
                                <a href="#">Worehouse</a>
                            </li>
                            <li>
                                <a href="#">Available balance</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
    </aside>
    <div class="content-page container-fluid">
        <?php include "views/".$this->view.".view.php";?>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
<!-- <script src="/public/vendor/jquery/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="/public/css/scriptephemere.js"></script>
</body>
</html>
