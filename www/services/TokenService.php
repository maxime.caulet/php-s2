<?php
namespace App\services;

use App\models\Users;

class TokenService {

    private static $instance;

    private function __construct() {}

    public static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new TokenService();
        }

        return self::$instance;
    }

    public function createToken($data) {
        $data['token'] = $this->generateToken();
        $user = Users::of($data);
        $this->saveToken($user);
    }

    public function updateToken() {
        if(isset($_SESSION['token'])) {
            $user = $this->findUserByToken();
            $newToken = $this->generateToken();
            $user->setToken($newToken);
            $this->saveToken($user);
        }
    }

    public function destroyToken(){
        $user = $this->findUserByToken();
        $user->setToken(null);
        
        $this->saveToken($user);
    }

    private function generateToken(){
        return bin2hex(random_bytes(64));
    }

    private function findUserByToken(){
        $user = new Users();
        $configGetOneBy = Users::getOneByToken();
        $result = $user->getOneBy($configGetOneBy);

        return Users::of($result);
    }

    private function saveToken($user) {
        $user->save();
        $_SESSION['token'] = $user->getToken();
    }
}