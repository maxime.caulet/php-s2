<?php
namespace App\controllers;

use App\models\Lessons as Lessons;
use App\core\View;
use App\controllers\CategorieController as Categories;

class LessonController
{
    public function defaultAction()
    {
        echo "Action default dans le controller comment";
    }
    public function listAction()
    {
        $lessons = new Lessons();
        $lessonsList = $lessons->getAll();
        $myView = new View("admin/lessons/listlessons", "back");
        $myView->assign("lessons", $lessonsList);
    }
    public function panelAction()
    {
        $lessons = new Lessons();
        $categories = new Categories();
        $categoriesList = $categories->swipperAction();
        $lessonsList = $lessons->getAll();
        $myView = new View("public/lessons/listlessons", "front");
        $myView->assign("lessons", $lessonsList);
        $myView->assign("categories", $categoriesList);
    }
    public function oneAction()
    {
        $lessons = new Lessons();
        $categories = new Categories();
        $categoriesList = $categories->swipperAction();
        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            //valeurs entrées
            $lessons->setId($_POST["id"]);
            $lessons->setTitle($_POST["title"]);
            $lessons->setIdCategorie($_POST["id_categorie"]);
            $lessons->setContent($_POST["editor"]);
            $lessons->save();
            // call function list pour retourner a la liste des lessons
            $this->listAction();
            exit;
        }
        //call config inside model LESSONS
        $configGetOneBy = Lessons::getLess();
        $lessonOne = $lessons->getOneBy($configGetOneBy);
        $myView = new View("admin/lessons/onelesson", "back");
        $myView->assign("lesson", $lessonOne);
        $myView->assign("categories", $categoriesList);
    }
    //admin action
    public function deleteAction()
    {
        $lessons = new Lessons();
        $configDelete = Lessons::getLess();
        $lessons->delete($configDelete);
        $lessonsList = $lessons->getAll();
        $myView = new View("admin/lessons/listlessons", "back");
        $myView->assign("lessons", $lessonsList);
    }
    public function uniqueAction()
    {
        $lessons = new Lessons();
        $configGetOneBy = Lessons::getLess();
        $lessonOne = $lessons->getOneBy($configGetOneBy);
        error_log(print_r($lessonOne,true));
        $myView = new View("public/lessons/onelesson", "front");
        $myView->assign("lesson", $lessonOne);
    }
}
