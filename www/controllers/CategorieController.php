<?php
namespace App\controllers;

use App\models\Categories;
use App\core\View;

class CategorieController
{
    public function defaultAction()
    {
        echo "Action default dans le controller page";
    }
    public function addAction()
    {
        echo "Action add dans le controller page";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller page";
    }
    public function updateAction()
    {
        echo "Action update dans le controller page";
    }
    public function listAction()
    {
        $categories = new Categories();
        $categoriesList = $categories->getAll();
        $myView = new View("admin/categories/listcategories", "back");
        $myView->assign("categories", $categoriesList);

    }
    public function swipperAction()
    {
        $categories = new Categories();
        $categoriesList = $categories->getAll();
        return $categoriesList;

    }
    public function panelAction()
    {
        $categories = new Categories();
        $categoriesList = $categories->getAll();
        $myView = new View("public/categories/listcategories", "front");
        $myView->assign("categories", $categoriesList);
    }
    public function oneAction()
    {
        $categories = new Categories();
        $configForm = Categories::getCategorieForm();
        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            //valeurs entrées
            $categories->setId($_POST["id"]);
            $categories->setName($_POST["name"]);
            $categories->SetContent($_POST["editor"]);
            $categories->SetOrder($_POST["order_page"]);
            $categories->save();
            // call function list pour retourner a la liste des pages
            $this->listAction();
            exit;
        }
        //call config inside model PAGE
        $configGetOneBy = Categories::getCategorie();
        $categorieOne = $categories->getOneBy($configGetOneBy);
        $myView = new View("admin/categories/onepage", "back");
        $myView->assign('configForm',$configForm); 
        $myView->assign("page", $categorieOne);
    }
}