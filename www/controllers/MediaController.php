<?php
namespace App\controllers;

class MediaController
{
    public function defaultAction()
    {
        echo "Action default dans le controller media";
    }
    public function addAction()
    {
        echo "Action add dans le controller media";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller media";
    }
    public function updateAction()
    {
        echo "Action update dans le controller media";
    }
}
