<?php
namespace App\controllers;

class PluginController
{
    public function defaultAction()
    {
        echo "Action default dans le controller plugin";
    }
    public function addAction()
    {
        echo "Action add dans le controller plugin";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller plugin";
    }
    public function updateAction()
    {
        echo "Action update dans le controller plugin";
    }
}