<?php
namespace App\controllers;

class CommentController
{
    public function defaultAction()
    {
        echo "Action default dans le controller comment";
    }
    public function addAction()
    {
        echo "Action add dans le controller comment";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller comment";
    }
    public function updateAction()
    {
        echo "Action update dans le controller comment";
    }
}
