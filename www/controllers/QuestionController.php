<?php
namespace App\controllers;

use App\models\Questions;
use App\models\Reponses;
use App\core\View;
use App\controllers\CategorieController as Categories;

class QuestionController
{
    public function defaultAction()
    {
        echo "Action default dans le controller comment";
    }
    public function listAction()
    {
        $questions = new Questions();
        $questionsList = $questions->getAll();
        $myView = new View("admin/questions/listquestions", "back");
        $myView->assign("questions", $questionsList);
    }
    public function oneAction()
    {
        $questions = new Questions();
        // Si la requete est POST
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //valeurs entrées
            $questions->setId($_POST["id"]);
            $questions->setIdExercice($_POST["title"]);
            $questions->setNumeroQuestion($_POST["title"]);
            $questions->setQuestion($_POST["editor"]);
            $questions->save();
            // call function list pour retourner a la liste des QUESTIONS
            $this->listAction();
            exit;
        }
        //call config inside model QUESTIONS
        $configGetOneBy = Questions::getQuest();
        $questionOne = $questions->getOneBy($configGetOneBy);
        $myView = new View("admin/questions/onequestion", "back");
        $myView->assign("question", $questionOne);
    }
    public function panelAction()
    {
        $questions = new Questions();
        $categories = new Categories();
        $categoriesList = $categories->swipperAction();
        $questionsList = $questions->getAll();
        $myView = new View("public/questions/listquestions", "front");
        $myView->assign("questions", $questionsList);
        $myView->assign("categories", $categoriesList);
    }
    public function uniqueAction()
    {
        $questions = new Questions();
        $configGetOneBy = Questions::getQuest();
        $questionOne = $questions->getOneBy($configGetOneBy);
        $myView = new View("public/questions/onequestion", "front");
        $myView->assign("question", $questionOne);
    }
    public function saveQuestionnaire($post)
    {
        $questions = new Questions();
        $reponses = new Reponses();
        $num_quest = 1;
        $num_rep = 1;
        error_log(print_r($post,true));
        foreach ($post as $key=>$value) {
            if (stristr($key, 'intitule_question_')!==false) {
                $questions->setIdExercice($_POST["id"]);
                $questions->setQuestion($value);
                $questions->setNumeroQuestion($num_quest);
                $num_quest = $num_quest + 1;
            }
            if (stristr($key, 'check_question_')!==false) {
                $reponses->setStatus($value);
            }
            if (isset($reponses->{'status'})) {
                $status_fixe = $reponses->{'status'};
            } else {
                $status_fixe = null;
            }
            if ($status_fixe == null) {
                $reponses->setStatus("0");
            }
            foreach ($post as $key_bis =>$value_bis) {
                if (stristr($key_bis, 'intit_question_')!==false) {
                    $reponses->setNumeroReponse($num_rep);
                    $reponses->setReponse($value_bis);
                    $reponses->save();
                    unset($reponses->status);
                    $num_rep = $num_rep + 1;
                    $questions->save();
                }
            }
        }
    }
}
