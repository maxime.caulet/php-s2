<?php
namespace App\controllers;

class TemplateController
{
    public function defaultAction()
    {
        echo "Action default dans le controller template";
    }
    public function addAction()
    {
        echo "Action add dans le controller template";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller template";
    }
    public function updateAction()
    {
        echo "Action update dans le controller template";
    }
}