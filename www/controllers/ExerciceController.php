<?php
namespace App\controllers;

use App\models\Exercices as Exercices;
use App\core\View;
use App\controllers\CategorieController as Categorie;
use App\controllers\QuestionController as Question;
use App\controllers\ReponseController as Reponse;
use App\models\Questions;
use App\models\Reponses;

class ExerciceController
{
    public function defaultAction()
    {
        echo "Action default dans le controller comment";
    }
    public function listAction()
    {
        $exercices = new Exercices();
        $exercicesList = $exercices->getAll();
        $myView = new View("admin/exercices/listexercices", "back");
        $myView->assign("exercices", $exercicesList);
    }
    public function panelAction()
    {
        $exercices = new Exercices();
        $categories = new Categorie();
        $categoriesList = $categories->swipperAction();
        $exercicesList = $exercices->getAll();
        $myView = new View("public/exercices/listexercices", "front");
        $myView->assign("exercices", $exercicesList);
        $myView->assign("categories", $categoriesList);
    }
    public function oneAction()
    {
        /* MOdels */
        $exercices = new Exercices();
        $questions = new Questions();
        $reponses = new Reponses();
        /* Controller */
        $question = new Question();
        $reponse = new Reponse();
        $categorie = new Categorie();
        /* Get all categories */
        $categoriesList = $categorie->swipperAction();

        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            //valeurs entrées
            $exercices->setId($_POST["id"]);
            $exercices->setTitle($_POST["title"]);
            $exercices->setIdCategorie($_POST["id_categorie"]);
            $exercices->setContent($_POST["editor"]);
            $question->saveQuestionnaire($_POST);
            //$reponse->saveQuestionnaire($_POST);
            $exercices->save();
            // call function list pour retourner a la liste des exercices
            $this->listAction();
            exit;
        }
        //call config inside model exercices
        $configGetOneBy = Exercices::getLess();
        $configGetQuestionnaire = Questions::getQuestionnaire();
        $exerciceOne = $exercices->getOneBy($configGetOneBy);
        $questionsList = $questions->getAllBy($configGetQuestionnaire);
        $reponsesList = $reponses->getAllBy($configGetQuestionnaire);
        //error_log(print_r($reponsesList,true));
        $myView = new View("admin/exercices/oneexercice", "back");
        $myView->assign("exercice", $exerciceOne);
        $myView->assign("categories", $categoriesList);
        $myView->assign("questions", $questionsList);
        $myView->assign("reponses", $reponsesList);
    }
    //admin action
    public function deleteAction()
    {
        $exercices = new Exercices();
        $configDelete = Exercices::getLess();
        $exercices->delete($configDelete);
        $exercicesList = $exercices->getAll();
        $myView = new View("admin/exercices/listexercices", "back");
        $myView->assign("exercices", $exercicesList);
    }
    public function uniqueAction()
    {
        $questions = new Questions();
        $exercices = new Exercices();
        $reponses = new Reponses();
        $configGetOneBy = Exercices::getLess();
        $configGetQuestionnaire = Questions::getQuestionnaire();
        $questionsList = $questions->getAllBy($configGetQuestionnaire);
        $reponsesList = $reponses->getAllBy($configGetQuestionnaire);
        $exerciceOne = $exercices->getOneBy($configGetOneBy);
        $data = $reponsesList;
        $myView = new View("public/exercices/oneexercice", "front");
        $myView->assign("exercice", $exerciceOne);
        $myView->assign("questions", $questionsList);
        $myView->assign("reponses", $reponsesList);
        $myView->assign("data", $data);
    }
}
