<?php
namespace App\controllers;

use App\models\Guests;
use App\core\View;

class DefaultController
{
    public function defaultAction()
    {
        $guest = new Guests();
        if(isset($_SESSION['guest_name'])){
            //nothing
        } else {
            $randSurname = Guests::getSurname();
            $guest->setSurname($randSurname);
            $guest->setIp($_SERVER['REMOTE_ADDR']);
            //$guest->save();
            $_SESSION['guest_name'] = $randSurname;
        }
        
        $name = "Maxime";
        $myView = new View("public/main","front");
        $myView->assign("name", $name);
        
    }
    public function indexAction()
    {
        $myView = new View("public/index","index");  
    }
    
}