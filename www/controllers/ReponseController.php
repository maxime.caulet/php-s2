<?php
namespace App\controllers;

use App\models\Lessons as Lessons;
use App\core\View;
use App\controllers\CategorieController as Categories;
use App\models\Reponses;
use Error;

class ReponseController
{
    public function defaultAction()
    {
        echo "Action default dans le controller comment";
    }

    public function saveQuestionnaire($post)
    {
        $reponses = new Reponses();
        $num_rep = 1;
        $num_quest = 1;
        error_log(print_r($post,true));
        foreach($post as $key=>$value)
        {   
            if($num_rep > 3){
                $num_rep = 1; 
                $num_quest  = $num_quest + 1;
            }
            error_log(print_r($reponses,true));
            error_log('key => '.$key.' | value => '.$value);
            $reponses->setIdExercice($_POST["id"]);
            if(stristr($key,'check_question_')!==FALSE){
                $reponses->setStatus($value);
            }
            if(isset($reponses->{'status'})){
                $status_fixe = $reponses->{'status'};
            } else {
                $status_fixe = NULL;
            }
            if($status_fixe == NULL){
                $reponses->setStatus("0");
            }
            if(stristr($key,'intit_question_')!==FALSE){
                $reponses->setNumeroQuestion($num_quest);
                $reponses->setNumeroReponse($num_rep);
                $reponses->setReponse($value);
                $reponses->save();
                unset($reponses->status);
                $num_rep = $num_rep + 1;
            }
            
        }
    }
}