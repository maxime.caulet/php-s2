<?php
namespace App\controllers;

use App\models\Astuces as Astuces;
use App\core\View;
use App\controllers\CategorieController as Categories;

class AstuceController
{
    public function defaultAction()
    {
        echo "Action default dans le controller comment";
    }
    public function listAction()
    {
        $astuces = new Astuces();
        $astucesList = $astuces->getAll();
        $myView = new View("admin/astuces/listastuces", "back");
        $myView->assign("astuces", $astucesList);
    }
    public function panelAction()
    {
        $astuces = new Astuces();
        $categories = new Categories();
        $categoriesList = $categories->swipperAction();
        $astucesList = $astuces->getAll();
        $myView = new View("public/astuces/listastuces", "front");
        $myView->assign("astuces", $astucesList);
        $myView->assign("categories", $categoriesList);
    }
    public function oneAction()
    {
        $astuces = new Astuces();
        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            //valeurs entrées
            $astuces->setId($_POST["id"]);
            $astuces->setTitle($_POST["title"]);
            $astuces->setContent($_POST["editor"]);
            $astuces->save();
            // call function list pour retourner a la liste des lessons
            $this->listAction();
            exit;
        }
        //call config inside model LESSONS
        $configGetOneBy = Astuces::getLess();
        $astuceOne = $astuces->getOneBy($configGetOneBy);
        $myView = new View("admin/astuces/oneastuce", "back");
        $myView->assign("astuce", $astuceOne);
    }
    //admin action
    public function deleteAction()
    {
        $astuces = new Astuces();
        $configDelete = Astuces::getLess();
        $astuces->delete($configDelete);
        $astucesList = $astuces->getAll();
        $myView = new View("admin/astuces/listastuces", "back");
        $myView->assign("astuces", $astucesList);
    }
    public function uniqueAction()
    {
        $astuces = new Astuces();
        $configGetOneBy = Astuces::getLess();
        $astuceOne = $astuces->getOneBy($configGetOneBy);
        $myView = new View("public/astuces/oneastuce", "front");
        $myView->assign("astuce", $astuceOne);
    }
}
