<?php
namespace App\controllers;

use App\models\Pages;
use App\core\View;

class PageController
{
    public function defaultAction()
    {
        echo "Action default dans le controller page";
    }
    public function addAction()
    {
        echo "Action add dans le controller page";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller page";
    }
    public function updateAction()
    {
        echo "Action update dans le controller page";
    }
    public function listAction()
    {
        $pages = new Pages();
        $pagesList = $pages->getAll();
        $myView = new View("admin/pages/listpages", "back");
        $myView->assign("pages", $pagesList);

    }
    public function oneAction()
    {
        $pages = new Pages();
        $configForm = Pages::getPageForm();
        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            //valeurs entrées
            $pages->setId($_POST["id"]);
            $pages->setName($_POST["name"]);
            $pages->SetContent($_POST["editor"]);
            $pages->SetOrder($_POST["order_page"]);
            $pages->save();
            // call function list pour retourner a la liste des pages
            $this->listAction();
            exit;
        }
        //call config inside model PAGE
        $configGetOneBy = Pages::getPage();
        $pageOne = $pages->getOneBy($configGetOneBy);
        $myView = new View("admin/pages/onepage", "back");
        $myView->assign('configForm',$configForm); 
        $myView->assign("page", $pageOne);
    }
}