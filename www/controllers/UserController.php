<?php
namespace App\controllers;

use App\models\Users;
use App\core\View;
use App\core\Validator;
use App\services\TokenService;

class UserController
{
    private $tokenService;

    public function __construct()
    {
        $this->tokenService = TokenService::getInstance();
    }

    public function defaultAction()
    {
        $myView = new View("admin/dashboard","back");
    }
    public function addAction()
    {
        echo "Action add dans le controller user";
    }
    public function deleteAction()
    {
        echo "Action delete dans le controller user";
    }
    public function oneAction()
    {
        $users = new Users();
        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            //valeurs entrées
            $users->setId($_POST["id"]);
            $users->setFirstname($_POST["firstname"]);
            $users->setLastname($_POST["lastname"]);
            $users->setEmail($_POST["email"]);
            $users->setPwd($_POST["pwd"]);
            $users->setStatus($_POST["status"]);
            $users->save();
            // call function list pour retourner a la liste des USERS
            $this->listAction();
            exit;
        }
        //call config inside model Users
        $configGetOneBy = Users::getUs();
        $userOne = $users->getOneBy($configGetOneBy);
        $myView = new View("admin/users/oneuser", "back");
        $myView->assign("user", $userOne);
    }
    public function listAction()
    {
        $users = new Users();
        $usersList = $users->getAll();
        $myView = new View("admin/users/listusers","back");
        $myView->assign("users", $usersList);
    }
    public function registerAction()
    {
        $configForm = Users::getRegisterForm();
        // Si la requete est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            $errors = Validator::formValidate( $configForm,$_POST );
            error_log(print_r($_POST,true));
            //Si $errors contient quelquechose 
            if(!empty($errors)){
                // annulation du post
                error_log('Validation du formulaire register invalide // annulation');
            } else {
                // enregistrement
                error_log('Validation du formulaire register valide // validation');
                //valeurs entrées
                $user = new Users();
                $user->setFirstname($_POST["firstname"]);
                $user->setLastname($_POST["lastname"]);
                $user->setEmail($_POST["email"]);
                $user->setPwd($_POST["password"]);
                $user->setStatus(0);
                $user->save();
            }
        }
        $myView = new View("login/register", "front");
        $myView->assign('configForm',$configForm);  
    }
    public function loginAction()
    {
        // verif si il est login
        // if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
        //    header("location: home"); 
        //    exit;
        // }
        $configForm = Users::getLoginForm();
        // Si la method est POST
        if( $_SERVER["REQUEST_METHOD"] == "POST"){
            $errors = Validator::formValidate( $configForm,$_POST );
            if(!empty($errors)){
                // annulation du post
                error_log('Validation du formulaire login invalide // annulation');
            } else {
                // enregistrement
                error_log('Validation du formulaire login valide // validation');
                //valeurs entrées
                $user = new Users();
                $configGetOneBy = Users::getOneUser();
                $result = $user->getOneBy($configGetOneBy);
                if(isset($result)){
                    error_log("login ok");
                    error_log(print_r($result,true));

                    foreach($result as $row => $key){
                        $_SESSION[$row] = $key;
                    } 
                    $_SESSION['logon'] = 1;

                    $this->tokenService->createToken($result);
                } else {
                    error_log("error login");
                }
            }
        }

        $myView = new View("login/login","account");
        $myView->assign('configForm',$configForm);  
    }
    public function logoutAction()
    {
        $this->tokenService->destroyToken();
        session_destroy();
        header('Location: /login');
    }
    public function forgotPwdAction()
    {
        $myView = new View("login/forgotPwd","account");
    }
}
