<?php

class Config
{
    public $host_pa = 'databaseCMS';
    public $username_pa = 'root';
    public $password_pa = 'password';
    public $db_pa = 'mvc-pa';
    public $db_pref_pa = 'dpom_';
    public $db_type_pa = 'mysql';

    // APPLICATION INFORMATION
    public $psa_path = "/psa";
    public $app_path = "/home/aslApps/asl_psa";

    // Session timeout
    public $session_timeout = 25 * 60;
}
?>
