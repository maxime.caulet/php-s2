<?php
use App\core\ConstLoader;
use App\services\TokenService; 

//Vieille fonction autoload
function myAutoload($class)
{
    if (file_exists("core/" . $class . ".class.php")) {
        include "core/" . $class . ".class.php";
    } elseif (file_exists("models/" . $class . ".model.php")) {
        include "models/" . $class . ".model.php";
    }
}
function myAutoloader($class)
{
    $class = str_replace('App', '', $class);
    $class = str_replace('\\', '/', $class);

    if($class[0] == '/') {
        include  substr($class.'.php', 1);
    } else {           
        include $class.'.php';
    }
}
session_start();
spl_autoload_register("myAutoloader");
new ConstLoader();

// TokenService::getInstance()->updateToken();

// Clean URL (obj:enlever les parametres pour la comparaison avec le fichier Route)
$uri = $_SERVER["REQUEST_URI"];
$parseUri = parse_url($uri);
$uriClean = $parseUri['path'];

$listOfRoutes = yaml_parse_file("routes.yml");

if (!empty($listOfRoutes[$uriClean])) {
    $c = 'App\controllers\\'.ucfirst($listOfRoutes[$uriClean]["controller"]."Controller");
    $a = $listOfRoutes[$uriClean]["action"] . "Action";
    new $c;
    //include "controllers/".$c.".php";
    if (class_exists($c)) {
        $controller = new $c();
        if (method_exists($controller, $a)) {
            $controller->$a();
        } else {
            die("L'action' n'existe pas");
        }
    } else {
        die("Le class controller n'existe pas");
    }
} else {
    header("location:views/404.html");
}