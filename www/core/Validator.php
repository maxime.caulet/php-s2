<?php
namespace App\core;

class Validator{
    public static function formValidate($config,$data)
    {
        $listOfErrors = [];
        //verifie le nombre de champs dans le formulaire
        if(count($config['fields']) == count($data)){
            //parse les champs contenu dans le table $config [$name = nom du champs | $configField = info diverse du champs]
            foreach ($config["fields"] as $name => $configField) {
                //verifier que les name des champs existent +  les required
                if (isset($data[$name]) && ($configField["required"] && !empty($data[$name]))) {

                    //verifier le format de lastname
                    if($configField["type"] == "text"){
                        // error_log('data lastname');
                        error_log($data[$name]);
                        //verifier minlenght lastname
                        if( strlen($data[$name]) >= $configField['minlenght']){
                            //verifier maxlenght lastname
                            if( strlen($data[$name]) <= $configField['maxlenght']){
                                //min lenght + max lenght respecter lastname
                            } else {
                                $listOfErrors [] = "lastname maxlenght erreur";
                            }
                        } else {
                            $listOfErrors [] = "lastname minlenght erreur";
                        }
                    }

                    // //verifier le format de firstname
                    // else if($configField["type"] == "firstname"){
                    //     // error_log('data firstname');
                    //     // error_log($data[$name]);
                    //     //verifier minlenght firstname
                    //     if( strlen($data[$name]) >= $configField[$data[$name]]['minlenght']){
                    //         //verifier maxlenght firstname                            
                    //         if( strlen($data[$name]) <= $configField[$data[$name]]['maxlenght']){
                    //             //min lenght + max lenght respecter firstname
                    //         } else {
                    //             $listOfErrors [] = "firstname maxlenght erreur";
                    //         }
                    //     } else {
                    //         $listOfErrors [] = "firstname minlenght erreur";
                    //     }
                    // }

                    //verifier le format de l'email
                    else if($configField["type"] == "email"){
                        // error_log('data email');
                        error_log($data[$name]);
                        if(self::emailValidate($data[$name])){
                            //verifier l'unicité de l'email
                        } else {
                            //$listOfErrors [] = $configField["errMsg"];
                            $listOfErrors [] = "email erreur";
                        }
                    }
                    //verifier le pwd
                    else if($configField["type"] == "password") {
                        error_log($data[$name]);
                        if(isset($configField['minlenght'])){
                            if( strlen($data[$name]) >= $configField['minlenght']){
                                //verifier l'unicité du pwd
                            } else {
                                $listOfErrors [] = "password minlenght erreur";
                            }
                        } else {
                            //Verifications avec le confirm password

                            // if($data[$name] == $data[$name]){
                            //     //verifier l'unicité de l'email
                            // } else {
                            //     $listOfErrors [] = "password erreur";
                            // }
                        }

                        
                    }
                    //verifier le pwd
                    else if($configField["type"] == "captcha"){
                        //error_log('data captcha');
                         error_log($data[$name]);
                        if($data[$name] == strtolower ($_SESSION['captcha'])){
                            //verifier l'unicité du captcha
                        } else {
                            $listOfErrors [] = "captcha erreur";
                        } 
                    }
                } else {
                    //nom des champs non compatible / ou required non respecte
                    $listOfErrors [] = "nom des champs non compatible / ou required non respecte";
                    return ["hack tentative"];
                }
            }
        } else { 
            //nombre incorrect de champs
            $listOfErrors [] = "nombre incorrect de champs";
            return ["hack tentative"];
        }
        return $listOfErrors;
    }
    public static function emailValidate($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}