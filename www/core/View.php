<?php
namespace App\core;

class View
{
    private $view;
    private $template;
    private $data = [];

    public function __construct($view, $template)
    {
        $this->setTemplate($template);
        $this->setView($view);
    }

    public function setTemplate($template)
    {
        $this->template = strtolower(trim($template));

        if (!file_exists("views/templates/" . $this->template . ".tpl.php")) {
            die("template not found.");
        }
    }

    public function setView($view)
    {
        $this->view = strtolower(trim($view));

        if (!file_exists("views/" . $this->view . ".view.php")) {
            die("view not found");
        }
    }

    public function assign($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function addModal($modal, $data)
    {
        if (!file_exists("views/modals/" . $modal . ".mod.php")) {
            die("modal : " . $modal . " not found");
        }

        include "views/modals/" . $modal . ".mod.php";
    }

    public function __destruct()
    {
        extract($this->data);
        include "views/templates/" . $this->template . ".tpl.php";
    }
}
