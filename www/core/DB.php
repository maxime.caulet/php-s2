<?php
namespace App\core;

class DB
{
    private $table;
    private $pdo;    

    //SINGLETON
    public function __construct()
    {
        $servername = "databaseCMS";
        $username = "root";
        $password = "password";
        //$password = "rootpasswd";
        $pref_db = "dpom_";
        
        // try {
        //     $this->pdo = new PDO(DRIVER_DB.":host=".HOST_DB.";dbname=".NAME_DB, USER_DB, PWD_DB);
        // } catch (Exception $e) {
        //     die("Erreur SQL : ".$e->getMessage());
        // }

        try {
            $this->pdo = New \PDO("mysql:host=$servername;dbname=mvc-pa", $username, $password);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            
        } catch (\Exception $e) {
			$msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
			die($msg);
		}

        //A completer
        $table_name = get_called_class();
        $table_name = str_replace('App', '', $table_name);
        $table_name = str_replace('models', '', $table_name);
        $table_name = str_replace('\\', '', $table_name);
        $table_name = strtolower($table_name);
        $this->table = $pref_db.$table_name;
        
    }
    public function save()
    {
        try {
            $objectVars = get_object_vars($this);
            $classVars = get_class_vars(get_class());
            $columsData = array_diff_key($objectVars, $classVars);
            $colums = array_keys($columsData);
            error_log(print_r($classVars,true));
            
            if (!is_numeric($this->id)) {
                //INSERT
                $sql = "INSERT INTO ".$this->table." (".implode(",", $colums).") VALUES (:".implode(",:", $colums).");";
                
            } else {
                //UPDATE
                foreach ($colums as $colum) {
                    $sqlUpdate[] =  $colum."=:".$colum;
                }
                $sql = "UPDATE ".$this->table." SET ".implode(",", $sqlUpdate)." WHERE id=:id;";
                //error_log($sql);

            }
            $queryPrepared = $this->pdo->prepare($sql);
            if($queryPrepared->execute($columsData)){
                echo "Enregistrement ok";
                
            } else{
                echo "Enregistrement erreur";
            }
        }
        catch (\Exception $exception)
        {
            error_log($exception->getMessage());
       
        }
    }
    public function delete($id)
    {
        try {
            $objectVars = get_object_vars($this);
            $classVars = get_class_vars(get_class());
            $columsData = array_diff_key($objectVars, $classVars);

            $sql = "DELETE FROM ".$this->table." WHERE id=".$id.";";
                
            $queryPrepared = $this->pdo->prepare($sql);
            error_log($sql);
            if($queryPrepared->execute()){
                echo "Suppression ok";  
            } else{
                echo "Suppression erreur";
            }
        }
        catch (\Exception $exception)
        {
            error_log($exception->getMessage());
       
        }
    }
    public function getOneBy($configGetOneBy)
    {
        try 
        {
            $objectVars = get_object_vars($this);
            $classVars = get_class_vars(get_class());
            $columsData = array_diff_key($objectVars, $classVars);
            $colums = array_keys($columsData);

            $sql = "SELECT ".implode(",", $colums)." FROM ".$this->table;
            //declaration d'une variable vide pour stocker les conditions
            $sqlcond = "";
            // Si nombre de condition en égal à  1
            if(count($configGetOneBy) == 1){
                //parse le table de config placé en parametre
                foreach($configGetOneBy as $keyconfigGetOneBy => $valueconfigGetOneBy) {
                    $sqlcond = $keyconfigGetOneBy." = ".$valueconfigGetOneBy;
                }
                $sql = $sql." WHERE ".$sqlcond;
            // Si nombre de condition est supérieur à 1
            } else if(count($configGetOneBy) > 1){
                //parse le table de config placé en parametre
                foreach($configGetOneBy as $keyconfigGetOneBy => $valueconfigGetOneBy) {
                    $sqlcond = $sqlcond.$keyconfigGetOneBy." = '".$valueconfigGetOneBy."' AND ";
                }
                //sert à enlever "AND" à la fin de la requête
                $sqlcond = substr($sqlcond, 0, -4);
                //concatène $sql et $sqlcond
                $sql = $sql." WHERE ".$sqlcond;
            } else {
                // sinon ne rien faire
            }
            $queryPrepared = $this->pdo->prepare($sql);
            if($queryPrepared->execute($columsData)){
                error_log("selection execute ok");
                //verification qu'il existe qu'un seul champs dans la bdd
                error_log(print_r($queryPrepared,true));
                if ($queryPrepared->rowCount() == 1) {
                    error_log("selection rowcount ok");
                    if ($row = $queryPrepared->fetch(\PDO::FETCH_ASSOC)) {
                        error_log(print_r($row,true));
                        error_log("selection fetch ok");
                        // Store result in variables
                        return $row;
                    } else {
                        // Display an error message (fetch erreur)
                        error_log("selection fetch erreur");
                    }
                } else {
                // Display an error message (rowcount erreur)
                error_log("selection rowcount erreur");
                }             
            } else{
                // Display an error message (execute error)
                error_log("selection execute erreur");
            }
        }
        catch (\Exception $exception)
        {
            error_log($exception->getMessage());
        }
    }
    public function getAll()
    {
        try 
        {
            $objectVars = get_object_vars($this);
            $classVars = get_class_vars(get_class());
            $columsData = array_diff_key($objectVars, $classVars);
            $colums = array_keys($columsData);

            $sql = "SELECT ".implode(",", $colums)." FROM ".$this->table;
            error_log($sql);    
            $queryPrepared = $this->pdo->prepare($sql);
            if($queryPrepared->execute($columsData)){
                error_log("selection execute ok");
                if ($rows = $queryPrepared->fetchAll()) {
                    error_log("selection fetch all ok");
                    // Send datas in array
                    return $rows;
                } else {
                // Display an error message (fetch erreur)
                error_log("selection fetch all erreur");
                }             
            } else{
                // Display an error message (execute error)
                error_log("selection execute erreur");
            }
        }
        catch (\Exception $exception)
        {
            error_log($exception->getMessage());
        }
    }
    public function getAllBy($configGetOneBy)
    {
        try 
        {
            $objectVars = get_object_vars($this);
            $classVars = get_class_vars(get_class());
            $columsData = array_diff_key($objectVars, $classVars);
            $colums = array_keys($columsData);

            $sql = "SELECT ".implode(",", $colums)." FROM ".$this->table;
            //declaration d'une variable vide pour stocker les conditions
            $sqlcond = "";
            // Si nombre de condition en égal à  1
            if(count($configGetOneBy) == 1){
                //parse le table de config placé en parametre
                foreach($configGetOneBy as $keyconfigGetOneBy => $valueconfigGetOneBy) {
                    $sqlcond = $keyconfigGetOneBy." = ".$valueconfigGetOneBy;
                }
                $sql = $sql." WHERE ".$sqlcond;
            // Si nombre de condition est supérieur à 1
            } else if(count($configGetOneBy) > 1){
                //parse le table de config placé en parametre
                foreach($configGetOneBy as $keyconfigGetOneBy => $valueconfigGetOneBy) {
                    $sqlcond = $sqlcond.$keyconfigGetOneBy." = '".$valueconfigGetOneBy."' AND ";
                }
                //sert à enlever "AND" à la fin de la requête
                $sqlcond = substr($sqlcond, 0, -4);
                //concatène $sql et $sqlcond
                $sql = $sql." WHERE ".$sqlcond;
            } else {
                // sinon ne rien faire
            }

            $queryPrepared = $this->pdo->prepare($sql);
            if($queryPrepared->execute($columsData)){
                error_log("selection execute ok");
                if ($rows = $queryPrepared->fetchAll(\PDO::FETCH_ASSOC)) {
                    error_log("selection fetch ok");
                    // Store result in variables
                   return $rows;
                } else {
                // Display an error message (fetch erreur)
                error_log("selection fetch erreur");
                }             
            } else{
                // Display an error message (execute error)
                error_log("selection execute erreur");
            }
        }
        catch (\Exception $exception)
        {
            error_log($exception->getMessage());
        }
    }   
    
}