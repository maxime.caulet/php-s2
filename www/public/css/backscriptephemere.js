$('#questions_created_form').hide();

//for use DATATABLES JQuery
$(document).ready( function () {
    $('#myTable').DataTable();
} );

function addQuestion(){
    //Declarations des variables
    var displaySetting = document.getElementById("questions_created_form").style.display;
    var valueSetting = document.getElementById("nb_question").value;
    var zone_question = document.getElementById("questions_created_form");
    //Affichage ou display de la partie QCM dans la creation
    if (displaySetting === "none" && valueSetting > 0) {
        // generateur de questions
        for (var i = 1; i <= valueSetting; i++) {
            zone_question.innerHTML += '<div id="quest_number_'+i+'"><fieldset><label for="question_detail">Question : <input type="text" name="intitule_question_'+i+'" size="80"> ?</label><p><input type="checkbox" name="check_question_1_'+i+'" value="1"><textarea id="intit_question_1_'+i+'" name="intit_question_1_'+i+'" rows="1" cols="50"></textarea><br><input type="checkbox" name="check_question_2_'+i+'" value="1"><textarea id="intit_question_2_'+i+'" name="intit_question_2_'+i+'" rows="1" cols="50"></textarea><br><input type="checkbox" name="check_question_3_'+i+'" value="1"><textarea id="intit_question_3_'+i+'" name="intit_question_3_'+i+'" rows="1" cols="50"></textarea><br></p></fieldset></div>'
        }
        $('#questions_created_form').show();
        $('#bouton_default').html('ANNULER LES QUESTIONS');
    } else {
        $('#questions_created_form').html('');
        $('#questions_created_form').hide();
        $('#nb_question').val('0');
        $('#bouton_default').html('AJOUTER DES QUESTIONS (QCM)');
    }
      
}

var someAmount = 10 // or any amount

// assuming you are looking to add additional divs with each iteration
// which is what I assume you mean by 'concatenate'
