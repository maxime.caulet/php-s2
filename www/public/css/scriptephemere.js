

//for use DATATABLES JQuery
$(document).ready( function () {
    $('#myTable').DataTable();
} );



$('.toggle-sidebar .toggle-bar button').click(function() {
    $('.left-sidebar, .content-page, .header, main, .main .container').toggleClass('extended');
    $('.auth').css({'display' : 'none'});
});


$('.left-sidebar .toggle-sidebar .setting button').click(function() {
    $('.auth').toggle();
});

$('.toolbar .btn').on("click", function() {
$('.toggle-bar').toggleClass('bar-toggle');
});

$(function() {
    // Chemins des images
    var bon = 'https://user.oc-static.com/files/386001_387000/386923.png',
        mauvais = 'https://user.oc-static.com/files/386001_387000/386924.png',
        question = 'https://user.oc-static.com/files/386001_387000/386922.png';
        
    // Image au chargement du QCM
    $('img').each(function() {
        $(this).attr('src', question);
    });
    
    // Dissimulation des réponses
    $('.reponse').hide();

    // Mise en forme des div du QCM
    var q = $('.question');
    q.css({'font':'1em / 1.5 sans-serif','background':'#ececec','border':'1px solid #999999','width':'calc(100% - 40px)','height':'350px','padding':'15px','margin':'20px','box-sizing':'border-box'});
    $('.texte').css({'float':'left','width':'90%'});
    $('img').css({'float':'right','margin-top':'0'});
    $('.button_rep_quest').css({'display':'inline-block','padding':'15px 25px','margin':'0 20px','background':'#ececec','font':'600 1em / 1.5 sans-serif','color':'black','text-decoration':'none','text-transform':'uppercase'});

    // Action au survol du lien « Tester les réponses »
    var test = $('.button_rep_quest');
    

    // Attention valeur evolutive : autant de fois qu'il y a de quest
    // Annule l'action par défaut au clic
    test.on({
        'click':function(e) {
            e.preventDefault();
            $('.reponse').show();
            if ($(':radio[id="r1"]:checked').val()) {
                $('#img1').attr('src', bon); 
                $('#reponse1').css('color', 'green');
            } else {
                $('#img1').attr('src', mauvais);
                $('#reponse1').css('color', 'red');
            }
            
        }	
    });
    
});
