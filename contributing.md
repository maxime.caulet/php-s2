# Contribuer à notre projet

Ce projet est un essai. Il a beaucoup à apprendre de ceux qui l'utilisent et le testent, tant sur le plan de son contenu, de son ergonomie ou de sa documentation. En fonction des rôles de chacun et de ses compétences, il y a toujours une façon d’y contribuer.

## Echanger sur le projet

Nous avons crée un groupe discord et slack afin de capitaliser les discussions et faciliter les échanges entre nous, avec un canal dédié à notre projet. N'hésitez pas à faire part de vos impressions, afin de discuter d'une amélioration...
Proposer des améliorations
Au delà de la discussion au sein du groupe, il existe plusieurs façon de proposer des améliorations, présentées ici par ordre croissant d’implication.

## Poster des remarques sur le projet

Gitlab est l'outil qui héberge le code de notre CMS, il comporte une partie pour assurer le suivi des demandes d'amélioration, de corrections d'erreurs, de retours des utilisateurs. Pour ajouter ses propres commentaires dans l'outil
Utiliser l'outil est en général un pré-requis pour savoir de quoi on parle
vérifier dans la liste des issues que le sujet n'est pas déjà traité. Si il l'est, il est possible de commenter l'issue, d'ajouter des éléments...
si le sujet n'est pas déjà traité, créer une nouvelle issue
avec votre propre compte sur gitlab. Recommandé car cela vous permettra de suivre facilement l'évolution de votre demande. Ou encore d'être contacté pour plus de précision.

de manière anonyme sur la page d'accueil du projet. Dans ce cas, merci d'être particulièrement attentifs à la description, puisque nous n'aurons pas de moyen de demander des précisions.

## Envoyer des modifications

Si certaines modifications vous viennent à l’esprit, il est tout à fait possible d'envoyer la partie modifiée par vos soin. Il est possible d'envoyer cette proposition de modification :
en tant qu'issue dans le dépôt Gitlab, en ajoutant le fichier correspondant en pièce jointe,
via un post dans le groupe mattermost, en ajoutant le fichier correspondant en pièce jointe,
par email (si vous le connaissez) à un des contributeurs du projet

## Contribuer directement au code

La meilleure solution pour contribuer reste bien sûr de participer directement à la rédaction du « code ».
Pré-requis :
* Il est nécessaire d'avoir les rudiments de l'utilisation de Git
* Il est vivement recommandé d'avoir docker/dockerToolbox d’installé sur sa machine pour travailler sur la structure des fichiers directement,
* Il est souhaitable d'avoir un compte sur gitlab ou github pour soumettre ses contributions.

## cloner le dépôt du projet

créer une nouvelle branche pour les modifications souhaitées
faire les modifications, les tester largement
faire une demande d'intégration (pull-request) sur le dépôt du projet
