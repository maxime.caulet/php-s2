Charte graphique CMS
-----------------------------------------
COLORS 
-----------------------------------------
Main color : #A0C9E8 (bleu fonc�)
Second color : #9DA0F5 (violet fonc�)

CTA : #9DF5EC (bleu-vert)
Lock-CTA: #8E8E8E (gris fonc�)

btn-violet: 7275C8;

Nuances possibles : 
#A3BFFF (violet clair)
#A3EFFF (bleu clair)

Bases + textes : 
#F5F5F5 (blanc)
#131313 (noir)

Body : #F2F2F2 (gris-blanc)

Warning : #FF8357 (orange pastel)
Fatal: #FF4759 (rouge pastel)
Valid�: #86EB90 (vert pastel)

-----------------------------------------
FONTS
-----------------------------------------

Titres : Oswald
Texte : Comfortaa

@import des deux fonts :
@import url('https://fonts.googleapis.com/css?family=Comfortaa:300,400,500,600,700|Oswald:300,400,500,600,700&display=swap');




